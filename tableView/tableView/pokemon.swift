//
//  pokemon.swift
//  tableView
//
//  Created by Santiago Pazmiño on 27/11/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import Foundation
import ObjectMapper
class Pokemon:Mappable{
    
    var pkName:String?
    var pkPeso:Double?
    var pkAltura:Double?
    var pkId:Int?
    var pkAbilities:String?
    var pkStats:String?
    var pkType:String?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        pkName <- map["name"]
        pkPeso <- map["weight"]
        pkAltura <- map["height"]
        pkId <- map["id"]
        pkAbilities <- map["abilities.0.ability.name"]
        pkStats <- map["stats.0.stat.name"]
        pkType <- map["types.0.type.name"]
        
    }
    
}
