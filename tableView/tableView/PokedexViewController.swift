//
//  PokedexViewController.swift
//  tableView
//
//  Created by Santiago Pazmiño on 5/1/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate{
    
    @IBOutlet weak var pokedexTableView: UITableView!
    
    let pokemonService = PokemonService()
    var pokemonArray:[Pokemon] = []
    var selectedPokemon = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        pokemonService.delegate = self
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        pokemonService.get20FirstPokemon()
    }
    
    func first20Pokemon(_ pokemon: [Pokemon]) {
        pokemonArray = pokemon
        pokedexTableView.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return pokemonArray.count
        default:
            return 5
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
            cell.fillData(pokemonArray[indexPath.row])
       //     cell.textLabel?.text = pokemonArray[indexPath.row].pkName
            return cell
        
    }
   // func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   //     selectedPokemon = indexPath.row
   // }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
         selectedPokemon = indexPath.row
        return indexPath
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! PokemonDetailViewController
        destination.pokemon = pokemonArray[selectedPokemon]
    }

    
    

    

}
