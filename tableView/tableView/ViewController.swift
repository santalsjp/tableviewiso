//
//  ViewController.swift
//  tableView
//
//  Created by Santiago Pazmiño on 27/11/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper


class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pesoLabel: UILabel!
    @IBOutlet weak var alturaLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func consultarButton(_ sender: Any) {
        
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/1/").responseObject { (response: DataResponse<Pokemon>) in
            let pokemon = response.result.value
            
            DispatchQueue.main.async {
                self.nameLabel.text = pokemon?.pkName
                self.alturaLabel.text = "\(pokemon?.pkAltura ?? 0)"
                self.pesoLabel.text = "\(pokemon?.pkPeso ?? 0)"
            }
           
    }
    }
    

    

}

