//
//  PokemonDetailViewController.swift
//  tableView
//
//  Created by Santiago Pazmiño on 8/1/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class PokemonDetailViewController: UIViewController {
    
    var pokemon:Pokemon?

    @IBOutlet weak var pkAbilitiesLabel: UILabel!
    @IBOutlet weak var pokemonImageView: UIImageView!
    
    @IBOutlet weak var pkStats: UILabel!
    @IBOutlet weak var pkType: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        title = pokemon?.pkName
        self.pkAbilitiesLabel.text = pokemon?.pkAbilities
        self.pkStats.text = pokemon?.pkStats
        self.pkType.text = pokemon?.pkType
        
        let pkService = PokemonService()
        pkService.getPokemonImage(id: (pokemon?.pkId)!) {
            (pkImage) in self.pokemonImageView.image = pkImage
        }

        // Do any additional setup after loading the view.
    }

    
}
