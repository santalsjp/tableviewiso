//
//  PokemonTableViewCell.swift
//  tableView
//
//  Created by Santiago Pazmiño on 5/1/18.
//  Copyright © 2018 Santiago Pazmiño. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(_ pokemon:Pokemon){
        idLabel.text = "\(pokemon.pkId ?? 0)"
        nameLabel.text = pokemon.pkName
    }

}
